# FAQ
[<span class="glyphicon glyphicon-arrow-left"></span> Retour à l'accueil](https://framagenda.org/doc)
#### Puis-je partager mes agendas avec des utilisateurs d'ownCloud ou de NextCloud qui ne sont pas sur Framagenda ?
Ce n'est actuellement pas possible. Le partage d'agendas entre utilisateurs n'est actuellement possible qu'au sein d'une même instance d'ownCloud ou de NextCloud.

#### Je connais un client fonctionnel qui n'est pas listé dans la liste des clients recommandés !
Merci de nous envoyer un message via https://contact.framasoft.org/ pour nous le signaler.

#### Pourquoi ne puis-je pas téléverser de fichiers sur Framagenda ?
Framagenda n'est pas un service d'hébergement de fichiers (voir [Framadrive](https://framadrive.org) à la place). Il est possible de téléverser de très petits fichiers (nous avons fixé la limite à 5MiB) mais cela est juste « toléré » si vous voulez mettre une petite pièce jointe à un événement.

#### Comment puis-je intégrer un calendrier public dans ma page web ?
Cliquez sur le lien public de l'agenda. En bas à gauche de la page, cliquez sur **Paramètres**, puis copiez le code en dessous de **Code pour intégrer une iframe**. Collez ce code dans le code html de votre site web à l'endroit où vous voulez positionner l'agenda.

Note : Pour des raisons de sécurité, le système de gestion de contenu Wordpress désactive l'utilisation des iframes. Vous pouvez utiliser un plugin comme [celui-ci](https://fr.wordpress.org/plugins/iframe/) pour résoudre ce problème.

#### Pourquoi la synchronisation des agendas doit se faire agenda par agenda sur Thunderbird ?
Thunderbird ne semble hélas pas supporter la découverte de tous agendas d'un utilisateur comme le font d'autres clients comme DAVdroid, il est donc nécessaire de récupérer l'adresse de chaque agenda pour le synchroniser.
