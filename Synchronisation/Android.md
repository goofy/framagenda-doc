# Synchronisation avec Android
[<span class="glyphicon glyphicon-arrow-left"></span> Retour à l'accueil](https://framagenda.org/doc)

Nous vous conseillons l'application **DAVdroid** pour la synchronisation sur Android. D'autres clients comme **CalDAV Sync Adapter** et **aCalDAV** sont succeptibles de fonctionner, mais DAVDroid a certains avantages par rapport aux autres applications.

## Installation

DAVdroid est disponible gratuitement sur [F-Droid](https://f-droid.org/repository/browse/?fdfilter=DavDroid&fdid=at.bitfire.davdroid) ([Tutoriel](../Divers/FDroid.md) d'installation de F-Droid) et au prix de 3.99€ (merci de soutenir son développeur) sur [Google Play](https://play.google.com/store/apps/details?id=at.bitfire.davdroid).

## Synchronisation des contacts et des agendas

* Ouvrez l'application DAVDroid
* Sélectionnez **Ajouter un compte** (le bouton + en bas à droite).
* Sélectionnez **Connexion avec une URL et un nom d'utilisateur**

![Ajouter un compte - DAVdroid](../img/davdroid-1.png)

* Entrez comme URL de base `https://framagenda.org/remote.php/dav/`, puis votre nom d'utilisateur ainsi que votre mot de passe sur Framagenda.
* Enfin, sélectionnez **Se connecter**.
* Il est conseillé de renseigner son adresse email pour vous identifier comme organisateur ou participant à des événements. Ne changez pas l'autre préférence.
* Une fois l'assistant terminé, cliquez sur le compte créé nouvellement dans DAVdroid.

![Contacts & Calendrier - DAVdroid](../img/davdroid-2.png)

Pour activer la synchronisation des contacts, appuyez simplement sur la case à cocher Contacts dans la section CardDAV.

Pour activer la synchronisation des agendas, cochez les cases correspondant à vos agendas dans la section CalDAV. Notez que l'agenda « Anniversaires des contacts » est en lecture seule.

Les agendas sont alors présents dans votre application d'Agenda.

![Agenda](../img/davdroid-5.png)

## Synchronisation des contacts existants du téléphone vers Framagenda

Les contacts déjà existants sur le téléphone ne sont pas synchronisés sur Framagenda car ils ne sont pas dans le même compte sur votre appareil Android. Il faut commencer par exporter vos contacts existants dans l'application Contacts de votre appareil, puis les réimporter dans le compte CalDAV.

Allez dans l'application Contacts, puis sélectionnez **Importer/Exporter** dans le menu. Choisissez **Exporter dans fichier VCF**, puis l'application vous demande où vous voulez enregistrer le fichier. Sélectionnez par exemple **Téléchargements**.

![Exportation des contacts](../img/davdroid-3.png)

Vous devez maintenant vous rendre dans vos téléchargements et ouvrir le fichier VCF. Il vous est demandé dans quel compte vous voulez importer ces contacts. Choisissez le compte DAVdroid que vous venez de créer. Les contacts sont alors importés dans ce compte et progressivement téléversés sur Framagenda. Si vous avez des contacts en double, vous pouvez sélectionner les comptes à afficher dans le menu de l'application Contacts : **Contacts à afficher**.

![Importation des contacts](../img/davdroid-4.png)

## Synchronisation de la liste de tâches

Pour synchroniser la liste des tâches de Framagenda sur votre appareil Android, vous devez installer l'application OpenTasks disponible gratuitement sur [F-Droid](https://f-droid.org/repository/browse/?fdfilter=OpenTasks&fdid=org.dmfs.tasks) et [Google Play](https://play.google.com/store/apps/details?id=org.dmfs.tasks).

Une fois l'application OpenTasks installée, DAVdroid détecte automatiquement la nouvelle installation et demande l'autorisation de lire les tâches afin de les synchroniser avec Framagenda.

Vous pouvez alors ouvrir l'application OpenTasks. Les agendas que vous gérés sont affichés et vous pouvez créer des tâches associées. Si vous créez d'autres listes de tâches, il faut les activer individuellement dans l'application DAVdroid (comme pour les agendas).

![OpenTasks interface](../img/tasks-android-1.png)

Pour créer une tâche, sélectionnez le bouton **+** en bas à droite de l'écran, puis sélectionnez l'endroit où vous voulez créer une tâche (le compte local de l'appareil, un de vos agendas ou bien une de vos listes de tâches).

![Apercu d'une tâche](../img/tasks-android-2.png)
