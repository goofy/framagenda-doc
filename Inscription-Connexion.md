# S'inscrire sur Framagenda
[<span class="glyphicon glyphicon-arrow-left"></span> Retour à l'accueil](https://framagenda.org/doc)

L'inscription à Framagenda nécessite une adresse de courriel.

Pour commencer, cliquez sur le lien **Créer un compte**.

![Framagenda-home](img/Framagenda-screen.png)

On vous demande de rentrer une adresse de courriel qui devra être confirmée par la réception d'un email.

![register-1](img/register-1.png)

![register-2](img/register-2.png)

Vous devez recevoir un email comme celui-ci.

![register-3](img/register-3.png)

En cliquant sur le lien contenu dans l'email, vous pouvez créer votre compte en choisissant un nom d'utilisateur et un mot de passe.

![register-4](img/register-4.png)

En validant, vous êtes inscrit et redirigé dans l'application.


# Se connecter à Framagenda

Pour vous connecter à Framagenda, remplissez les champs avec vos informations de connexion.

![Framagenda-connexion](img/Framagenda-connexion.png)
